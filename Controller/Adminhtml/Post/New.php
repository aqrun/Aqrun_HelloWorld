<?php
namespace Aqrun\HelloWorld\Controller\Adminhtml\Post;

class New extends \Magento\Backend\App\Action
{

    protected $_pageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ){
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->_pageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend('Add New Post');
        return $resultPage;
    }
}