<?php
namespace Aqrun\HelloWorld\Controller\Adminhtml\Post;

use Magento\Framework\View\Element\UiComponent\DataProvider\CollectionFactory;

class Test extends \Magento\Backend\App\Action
{
    protected $_resultPageFactory;
    protected $collectionFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        CollectionFactory $collectionFactory
    ){
        $this->_resultPageFactory = $resultPageFactory;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        echo '<pre>';
        var_dump($this->collectionFactory);exit;
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend('Test');

        return $resultPage;
    }
}