<?php
namespace Aqrun\HelloWorld\Plugin;

use Aqrun\HelloWorld\Controller\Index\Example as ExampleAction;

class ExamplePlugin
{
    public function beforeSetTitle(ExampleAction $subject, $title)
    {
        $title = $title . ' 来到 ';
        echo __METHOD__ . '</br>';
        return [$title];
    }

    public function afterGetTitle(ExampleAction $subject, $result)
    {
        echo __METHOD__ . '</br>';
        return '<h1>' . $result . '新世界' . '</h1>';
    }

    public function aroundGetTitle(ExampleAction $subject, callable $proceed)
    {
        echo __METHOD__ . ' - proceed() 之前 <br/>';
        $result = $proceed();
        echo __METHOD__ . ' - proceed() 之后 <br/>';
        return $result;
    }
}