<?php
namespace Aqrun\HelloWorld\Model;

class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'aqrun_helloworld_post';
    protected $_cacheTag = 'aqrun_helloworld_post';
    protected $_eventPrefix = 'aqrun_helloworld_post';

    protected function _construct()
    {
        $this->_init(\Aqrun\HelloWorld\Model\ResourceModel\Post::class);
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        return [];
    }
}