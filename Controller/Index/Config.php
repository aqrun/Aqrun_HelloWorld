<?php
namespace Aqrun\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Context;

class Config extends \Magento\Framework\App\Action\Action
{
    protected $helperData;

    public function __construct(
        Context $context,
        \Aqrun\HelloWorld\Helper\Data $helperData
    ){
        $this->helperData = $helperData;
        return parent::__construct($context);
    }

    public function execute()
    {
        echo $this->helperData->getGeneralConfig('enable');
        echo '<br/>';
        echo $this->helperData->getGeneralConfig('display_text');
        exit;
    }
}