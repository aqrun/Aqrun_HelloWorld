<?php
namespace Aqrun\HelloWorld\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

class Sayhello extends Command
{

    const NAME = 'name';

    protected function configure()
    {
        $options = [
            new InputOption(self::NAME, null,InputOption::VALUE_REQUIRED, '姓名')
        ];

        $this->setName('example:sayhello')
            ->setDescription('示例命令')
            ->setDefinition($options);
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($name = $input->getOption(self::NAME)) {
            $output->writeln('你好' . $name);
        } else {
            $output->writeln('你好世界');
        }
    }
}