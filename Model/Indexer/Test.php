<?php
namespace Aqrun\HelloWorld\Model\Indexer;

class Test implements \Magento\Framework\Indexer\ActionInterface,
    \Magento\Framework\Mview\ActionInterface
{
    /**
     * 被 mview 调用，允许在 "Update on schedule" 模式下处理索引
     * @param int[] $ids
     */
    public function execute($ids){
        //code
    }

    /**
     * 会处理所有的数据并重建索引
     * 在使用命令行重建索引时会执行
     */
    public function executeFull(){
        //code
    }

    /**
     * 作用于一组实体的变化（如批量操作）
     * @param array $ids
     */
    public function executeList(array $ids){
        //code
    }

    /**
     * 作用于单个实体
     * @param int $id
     */
    public function executeRow($id){
        // code
    }
}