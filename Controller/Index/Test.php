<?php
namespace Aqrun\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Action;

class Test extends Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ){
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $textDisplay = new \Magento\Framework\DataObject(['text' => '修改前要显示的文字']);
        $this->_eventManager->dispatch('aqrun_helloworld_display_text',
            ['textDisplay' => $textDisplay]);
        echo $textDisplay->getText();
        exit;
    }

}