<?php
namespace Aqrun\HelloWorld\Controller\Index;

class Example extends \Magento\Framework\App\Action\Action
{
    protected $title;
    public function execute()
    {
        $this->setTitle('欢迎');
        echo $this->getTitle();
        exit();
    }

    public function setTitle($title){
        echo __METHOD__ . '<br/>';
        return $this->title = $title;
    }

    public function getTitle()
    {
        echo __METHOD__ . '<br/>';
        return $this->title;
    }
}