<?php
namespace Aqrun\HelloWorld\Observer;

class ChangeDisplayText implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ){
        $textDisplay = $observer->getData('textDisplay');
        echo $textDisplay->getText() . " - 事件处理中 <br/>";
        $textDisplay->setText('事件处理成功，内容已经修改');

        return $this;
    }
}